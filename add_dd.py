#!/usr/bin/env python3
# -*- coding=utf-8 -*- 

import numpy as np
import pandas as pd

def get_data(fName):
    df = pd.read_json(fName)
    df.value = df.value.str.replace(
            ',', '').str.replace('$', '').fillna(
            value=-1).astype(int)
    df['index'] = df.index
    return df

def part_numbers(df, part, col):
        return df[df['round']==part][df.show_number==**show**][col].values.tolist()

def show_array(nums):
    return np.reshape(nums, (5, 6))


def find_dd(arr):
    for y, row in enumerate(arr):
        vals, locs = np.unique(row, return_counts=True)
        if 1 in locs:
            x = row.tolist().index(vals[locs.tolist().index(1)])
            return x, y


def run():
    df = get_data('JEOPARDY_QUESTIONS1.json')
    dd = []
    parts = ['Jeopardy!', 'Double Jeopardy!']
    cols = ['value', 'index']

    inds = []
    for show in df.show_numbers.unique():
        for part in parts:
            part_nums = part_numbers(df, part, 'value')
            arr = show_array(part_nums)
            loc = find_dd(arr)  # mask array to 1 and 0 from this?


if __name__=='__main__':
    run()
