#!/usr/bin/env python3
# -*- coding=utf-8 -*-

import requests
from bs4 import BeautifulSoup

import re
import json

class JeopardyScraper:
    def __init__(self):
        self.questions = []


    def soupify(self, link):
        response = requests.get(link)
        return BeautifulSoup(response.text, 'html.parser')

    def _title_info(self, soup):
        title = soup.title.text.split(' ')
        self.ep = int(title[4][1:-1])
        self.air_date = title[-1]


    def _round_categories(self):
        cats = [cat.find('td', {'class' : 'category_name'}).text 
                for cat in self.part.find_all('td', {'class' : 'category'})]

        return {i+1:cats[i] for i in range(len(cats))}


    def _question(self):
        q = self.clue.find('td', {'class' : 'clue_text'})
        if q:
            self.question =  q.text, 
            self.qID = q['id']


    def _value(self):
        val = self.clue.find('td', {'class' : re.compile('clue_valu*')})

        try:
            self.value = int(''.join(i for i in val.text if i.isdigit()))
        except:
            return

        if 'DD' in val.text:
            self.is_daily_double = True
        else:
            self.is_daily_double = False


    def _order(self):
        order = self.clue.find('td', {'class' : 'clue_order_number'})
        if order:
            self.order = order.text


    def _answer(self):
        try:
            toggle = self.clue.find('div').get('onmouseover')
            start = '''<em class="correct_response">'''
            end = '</em>'

            self.answer = toggle[toggle.find(start)+len(start):toggle.find(end)]
        except:
            self.answer = 'UNKNOWN_ERROR'

    def _category(self):
        cat_num = self.qID.split('_')[2]
        return self.categories[int(cat_num)]


    def add_round(self, soup, current_round, season):
        self.part = soup.find('div', {'id' : current_round})
        tbl = self.part.find('table')
        self.categories = self._round_categories()

        questions = []
        for clue in tbl.find_all('td', {'class' : 'clue'}):
            self.clue = clue
            self._title_info(soup)
            self._question()
            if not self.question:
                continue

            self._value()

            self._order()
            self._answer()

            category = self._category()

            self.questions.append({
                'episode'   : self.ep,
                'air_date'  : self.air_date,
                'round'     : current_round,
                'category'  : category,
                'question'  : self.question,
                'answer'    : self.answer,
                'value'     : self.value,
                'order'     : self.order,
                'is_dd'     : self.is_daily_double,
                'season'    : season,
            })


def run():
    inFile = './outputs/game_links.json'
    with open(inFile) as f:
        game_links = json.load(f)

    questions = []
    errors = []
    for season, links in game_links.items():
        scrapy = JeopardyScraper()
        for link in links:
            if link.startswith('http'):
                game = scrapy.soupify(link)
            else:
                continue

            try:
                scrapy.add_round(game, 'jeopardy_round', season)
                scrapy.add_round(game, 'double_jeopardy_round', season)
                # scrapy.add_final(game)

                questions += scrapy.questions
            except Exception as e:
                errors.append([season, link[-4:]])
                continue

        outFile = f'./outputs/season{season}_jeopardy_questions.json'
        with open(outFile, 'w') as f:
            json.dump(questions, f)
        print('File saved')

    error_log = './outputs/error.log'
    with open(error_log) as f:
        f.writelines('\n'.join(['\t'.join(row) for row in errors]))


if __name__=='__main__':
    run()
