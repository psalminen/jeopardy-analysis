#!/usr/bin/env python3
# -*- coding=utf-8 -*-

import requests
from bs4 import BeautifulSoup

import json

def get_soup(link):
    response = requests.get(link)
    return BeautifulSoup(response.text, 'html.parser')


def season_pages(start=1, end=36):
    url = 'http://www.j-archive.com/showseason.php?season='
    season_links = [f'{url}{s}' for s in range(start, end)]

    game_links = {}
    for season in range(start, end):
        link = f'{url}{season}'
        soup = get_soup(link)

        game_links[season] = [l['href'] for l in soup.findAll('a') if 'game_id' in l['href'] and 'http' in l['href']]

    return game_links

def run():
    links = season_pages()
    
    outFile = './game_links.json'
    with open(outFile, 'w') as f:
        json.dump(links, f)

if __name__=='__main__':
    run()
